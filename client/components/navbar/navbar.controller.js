'use strict';

angular.module('proyectoFullstackApp')
    .controller('NavbarCtrl', function ($scope, $location) {
        $scope.menu = [
            {
                'title': 'Home',
                'link': '/'
            },
            {
                'title': 'Scope',
                'link': '/scope-test'
            },
            {
                'title': 'Login',
                'link': '/login'
            }
        ];

        $scope.isCollapsed = true;

        $scope.isActive = function (route) {
            return route === $location.path();
        };
    });