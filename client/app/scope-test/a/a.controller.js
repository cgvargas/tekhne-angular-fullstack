'use strict';

angular.module('proyectoFullstackApp')
    .controller('ControllerA', function ($scope) {
        var vm = this;
        vm.propiedad = 'Valor A';
        vm.enviarMensaje = function() {
            $scope.$broadcast('mensaje-para-c', {
                asunto: 'Saludos desde A'
            });
        };

        $scope.$on('mensaje-para-a',
            function(event, datos){
                console.log('[A] mensaje recibido', datos);
            });
    });
