'use strict';

angular.module('proyectoFullstackApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.java.detail', {
        url: '/detail?id&name',
        templateUrl: 'app/main/java-detail/java-detail.html',
        controller: 'JavaDetailCtrl as javaDetail'
      });
  });